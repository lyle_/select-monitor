# Subaru Select Monitor Scan Tool

The Select Monitor Scan Tool is designed to read and display real-time data on
a Windows PC directly from a Subaru ECU.

This fork has been converted from Delphi to [Lazarus](http://www.lazarus-ide.org/).
At present, it doesn't build correctly.

Forked from: https://sourceforge.net/projects/selectmonitor/
Site: http://www.vwrx.com/index.php?pg=selectmonitor

